﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using VanPK.NWEB.Practice.T04.DataLayer.Context;

namespace VanPK.NWEB.Practice.T04.BusinessLayer.Infrastructures
{
    public abstract class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly CustomedDbContext dbContext;
        protected DbSet<TEntity> entities;

        public GenericRepository(CustomedDbContext dbContext)
        {
            this.dbContext = dbContext;
            entities = dbContext.Set<TEntity>();
        }
        public void Add(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(params object[] ids)
        {
            throw new NotImplementedException();
        }

        public TEntity Find(object id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TEntity> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Update(TEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
