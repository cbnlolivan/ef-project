﻿namespace VanPK.NWEB.Practice.T04.BusinessLayer.Infrastructures
{
    public interface IGenericRepository<T> where T : class
    {
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(params object[] ids);
        T Find(object id);
        IEnumerable<T> GetAll();
    }
}
