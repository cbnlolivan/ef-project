﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VanPK.NWEB.Practice.T04.DataLayer.Entities;

namespace VanPK.NWEB.Practice.T04.DataLayer.Initializer
{
    public static class Initializer
    {
        public static void Init(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserRole>().HasData(new List<UserRole>() {
                new UserRole()
                {
                    RoleId = new Guid("Role 1"),
                    RoleName = "Admin"
                },

                new UserRole()
                {
                    RoleId = new Guid("Role 2"),
                    RoleName = "Class Admin"
                },

                new UserRole()
                {
                    RoleId = new Guid("Role 3"),
                    RoleName = "Student"
                }
            });

            modelBuilder.Entity<Classroom>().HasData(new List<Classroom>() {
                new Classroom() 
                {
                    ClassId = new Guid("Class 1"),
                    ClassName = "KS05"
                },
                new Classroom()
                {
                    ClassId = new Guid("Class 2"),
                    ClassName = "KS04"
                },
                new Classroom()
                {
                    ClassId = new Guid("Class 3"),
                    ClassName = "K16 SE"
                }
            });

            modelBuilder.Entity<User>().HasData(new List<User>() {
                new User()
                {
                    UserId = new Guid("User 1"),
                    UserName = "Pham Khai Van",
                    Email = "cbnlolivan@gmail.com",
                    Password = "naviank",
                    PhoneNumber = "0384478185",
                    ClassId = Guid.Parse("Class 1"),
                },
            }); ;

            modelBuilder.Entity<Request>().HasData(new List<Request>(){
                new Request()
                {
                    RequestId = Guid.Parse("Request 1"),
                    UserId = Guid.Parse(""),
                },
            });
        }
    }
}
