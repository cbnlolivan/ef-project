﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VanPK.NWEB.Practice.T04.DataLayer.Entities;

namespace VanPK.NWEB.Practice.T04.DataLayer.Config
{
    public class ClassConfig : IEntityTypeConfiguration<Classroom>
    {
        public void Configure(EntityTypeBuilder<Classroom> builder)
        {
            builder.ToTable("Class");
            builder.HasKey(x => x.ClassId);
            builder.Property(x => x.ClassId).ValueGeneratedOnAdd();
            builder.Property(x => x.ClassName).IsRequired();

            builder.HasMany<User>(u => u.Users)
                .WithOne(c => c.Classroom).HasForeignKey(u => u.ClassId);
        }
    }
}
