﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using VanPK.NWEB.Practice.T04.DataLayer.Entities;

namespace VanPK.NWEB.Practice.T04.DataLayer.Config
{
    internal class RequestConfig : IEntityTypeConfiguration<Request>
    {
        public void Configure(EntityTypeBuilder<Request> builder)
        {
            builder.ToTable("Request");
            builder.HasKey(x => x.RequestId);
            builder.Property(x => x.RequestId).ValueGeneratedOnAdd();
            builder.Property(x => x.Reason).IsRequired();
        }
    }
}