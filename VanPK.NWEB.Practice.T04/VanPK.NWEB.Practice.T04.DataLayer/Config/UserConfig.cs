﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VanPK.NWEB.Practice.T04.DataLayer.Entities;

namespace VanPK.NWEB.Practice.T04.DataLayer.Config
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("User");
            builder.HasKey(x => x.UserId);
            builder.Property(x => x.UserId).ValueGeneratedOnAdd();
            builder.Property(x => x.UserName).IsRequired();
            builder.Property(x => x.Password).IsRequired();
            builder.Property(x => x.UserName).IsRequired();

            //builder.HasOne<Classroom>(u => u.Classroom)
            //    .WithMany(c => c.Users).HasForeignKey(u => u.ClassId);

            //builder.HasOne<UserRole>(u => u.Role)
            //    .WithMany(r => r.Users).HasForeignKey(u => u.RoleId);

            builder.HasMany<Request>(u => u.Requests)
                .WithOne(r => r.User).HasForeignKey(u => u.UserId);
        }
    }
}
