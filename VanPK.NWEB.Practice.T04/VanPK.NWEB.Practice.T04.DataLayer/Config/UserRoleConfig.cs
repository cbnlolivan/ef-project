﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VanPK.NWEB.Practice.T04.DataLayer.Entities;

namespace VanPK.NWEB.Practice.T04.DataLayer.Config
{
    public class UserRoleConfig : IEntityTypeConfiguration<UserRole>
    {
        public void Configure(EntityTypeBuilder<UserRole> builder)
        {
            builder.ToTable("UserRole");
            builder.HasKey(x => x.RoleId);
            builder.Property(x => x.RoleId).ValueGeneratedOnAdd();
            builder.Property(x => x.RoleName).IsRequired();

            builder.HasMany<User>(r => r.Users)
                .WithOne(x => x.Role).HasForeignKey(u => u.RoleId);
        }
    }
}
