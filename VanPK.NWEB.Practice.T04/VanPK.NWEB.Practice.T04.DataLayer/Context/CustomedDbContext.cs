﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VanPK.NWEB.Practice.T04.DataLayer.Config;
using VanPK.NWEB.Practice.T04.DataLayer.Entities;

namespace VanPK.NWEB.Practice.T04.DataLayer.Context
{
    public class CustomedDbContext : IdentityDbContext
    //public class CustomedDbContext : DbContext
    {
        public DbSet<UserRole> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<Classroom> Classrooms { get; set; }
        public CustomedDbContext()
        { }
        public CustomedDbContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new RequestConfig());
            modelBuilder.ApplyConfiguration(new UserConfig());
            modelBuilder.ApplyConfiguration(new ClassConfig());
            modelBuilder.ApplyConfiguration(new UserRoleConfig());

            //modelBuilder.Init();
        }
    }
}
