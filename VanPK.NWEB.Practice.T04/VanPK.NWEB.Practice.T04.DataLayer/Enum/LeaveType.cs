﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VanPK.NWEB.Practice.T04.DataLayer.Enum
{
    public enum LeaveType
    {
        LateAttend,
        EarlyLeave,
        AClassAbsent,
        WholeDayAbsent
    }
}
