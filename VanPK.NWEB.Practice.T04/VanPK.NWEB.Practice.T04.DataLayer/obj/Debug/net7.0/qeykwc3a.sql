﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [Class] (
    [ClassId] uniqueidentifier NOT NULL,
    [ClassName] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_Class] PRIMARY KEY ([ClassId])
);
GO

CREATE TABLE [UserRole] (
    [RoleId] uniqueidentifier NOT NULL,
    [RoleName] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_UserRole] PRIMARY KEY ([RoleId])
);
GO

CREATE TABLE [User] (
    [UserId] uniqueidentifier NOT NULL,
    [UserName] nvarchar(max) NOT NULL,
    [Password] nvarchar(max) NOT NULL,
    [Email] nvarchar(max) NOT NULL,
    [PhoneNumber] nvarchar(max) NOT NULL,
    [ClassId] uniqueidentifier NOT NULL,
    [RoleId] uniqueidentifier NOT NULL,
    CONSTRAINT [PK_User] PRIMARY KEY ([UserId]),
    CONSTRAINT [FK_User_Class_ClassId] FOREIGN KEY ([ClassId]) REFERENCES [Class] ([ClassId]) ON DELETE CASCADE,
    CONSTRAINT [FK_User_UserRole_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [UserRole] ([RoleId]) ON DELETE CASCADE
);
GO

CREATE TABLE [Request] (
    [RequestId] uniqueidentifier NOT NULL,
    [UserId] uniqueidentifier NOT NULL,
    [Title] nvarchar(max) NOT NULL,
    [Date] datetime2 NOT NULL,
    [Reason] nvarchar(max) NOT NULL,
    [LeaveType] int NOT NULL,
    [Status] int NOT NULL,
    CONSTRAINT [PK_Request] PRIMARY KEY ([RequestId]),
    CONSTRAINT [FK_Request_User_UserId] FOREIGN KEY ([UserId]) REFERENCES [User] ([UserId]) ON DELETE CASCADE
);
GO

CREATE INDEX [IX_Request_UserId] ON [Request] ([UserId]);
GO

CREATE INDEX [IX_User_ClassId] ON [User] ([ClassId]);
GO

CREATE INDEX [IX_User_RoleId] ON [User] ([RoleId]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20231213120535_init', N'7.0.14');
GO

COMMIT;
GO

