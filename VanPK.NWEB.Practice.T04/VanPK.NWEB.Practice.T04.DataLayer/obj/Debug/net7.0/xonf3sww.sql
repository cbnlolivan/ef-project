﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [Class] (
    [ClassId] uniqueidentifier NOT NULL,
    [ClassName] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_Class] PRIMARY KEY ([ClassId])
);
GO

CREATE TABLE [UserRole] (
    [RoleId] uniqueidentifier NOT NULL,
    [RoleName] nvarchar(max) NOT NULL,
    CONSTRAINT [PK_UserRole] PRIMARY KEY ([RoleId])
);
GO

CREATE TABLE [User] (
    [UserId] uniqueidentifier NOT NULL,
    [UserName] nvarchar(max) NOT NULL,
    [Password] nvarchar(max) NOT NULL,
    [Email] nvarchar(max) NOT NULL,
    [PhoneNumber] nvarchar(max) NOT NULL,
    [ClassId] uniqueidentifier NOT NULL,
    [RoleId] uniqueidentifier NOT NULL,
    [ClassroomClassId] uniqueidentifier NOT NULL,
    [UserRoleRoleId] uniqueidentifier NULL,
    CONSTRAINT [PK_User] PRIMARY KEY ([UserId]),
    CONSTRAINT [FK_User_Class_ClassId] FOREIGN KEY ([ClassId]) REFERENCES [Class] ([ClassId]) ON DELETE CASCADE,
    CONSTRAINT [FK_User_Class_ClassroomClassId] FOREIGN KEY ([ClassroomClassId]) REFERENCES [Class] ([ClassId]) ON DELETE CASCADE,
    CONSTRAINT [FK_User_UserRole_UserRoleRoleId] FOREIGN KEY ([UserRoleRoleId]) REFERENCES [UserRole] ([RoleId])
);
GO

CREATE TABLE [Request] (
    [RequestId] uniqueidentifier NOT NULL,
    [UserId] uniqueidentifier NOT NULL,
    [Title] nvarchar(max) NOT NULL,
    [Date] datetime2 NOT NULL,
    [Reason] nvarchar(max) NOT NULL,
    [LeaveType] int NOT NULL,
    [Status] int NOT NULL,
    CONSTRAINT [PK_Request] PRIMARY KEY ([RequestId]),
    CONSTRAINT [FK_Request_User_UserId] FOREIGN KEY ([UserId]) REFERENCES [User] ([UserId]) ON DELETE CASCADE
);
GO

CREATE INDEX [IX_Request_UserId] ON [Request] ([UserId]);
GO

CREATE INDEX [IX_User_ClassId] ON [User] ([ClassId]);
GO

CREATE INDEX [IX_User_ClassroomClassId] ON [User] ([ClassroomClassId]);
GO

CREATE INDEX [IX_User_UserRoleRoleId] ON [User] ([UserRoleRoleId]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20231213110741_init', N'7.0.14');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

ALTER TABLE [User] DROP CONSTRAINT [FK_User_Class_ClassroomClassId];
GO

ALTER TABLE [User] DROP CONSTRAINT [FK_User_UserRole_UserRoleRoleId];
GO

DROP INDEX [IX_User_ClassroomClassId] ON [User];
GO

DROP INDEX [IX_User_UserRoleRoleId] ON [User];
GO

DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[User]') AND [c].[name] = N'ClassroomClassId');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [User] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [User] DROP COLUMN [ClassroomClassId];
GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[User]') AND [c].[name] = N'UserRoleRoleId');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [User] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [User] DROP COLUMN [UserRoleRoleId];
GO

CREATE INDEX [IX_User_RoleId] ON [User] ([RoleId]);
GO

ALTER TABLE [User] ADD CONSTRAINT [FK_User_UserRole_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [UserRole] ([RoleId]) ON DELETE CASCADE;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20231213115404_initDB', N'7.0.14');
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20231213115929_naviank', N'7.0.14');
GO

COMMIT;
GO

