﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VanPK.NWEB.Practice.T04.DataLayer.Entities
{
    public class Classroom
    {
        public Guid ClassId { get; set; }
        public string ClassName { get; set; }
        public List<User> Users { get; set; }
    }
}
