﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VanPK.NWEB.Practice.T04.DataLayer.Enum;

namespace VanPK.NWEB.Practice.T04.DataLayer.Entities
{
    public class Request
    {
        public Guid RequestId { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }

        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string Reason { get; set; }
        public LeaveType LeaveType { get; set; }
        public RequestStatus Status { get; set; }


    }
}
