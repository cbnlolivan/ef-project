﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VanPK.NWEB.Practice.T04.DataLayer.Entities
{
    public class User
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public Guid ClassId { get; set; }
        public Classroom Classroom { get; set; }

        public Guid RoleId { get; set; }
        public UserRole Role { get; set; }

        public List<Request> Requests { get; set; }
    }
}
