﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VanPK.NWEB.Practice.T04.DataLayer.Entities
{
    public class UserRole
    {
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public List<User> Users { get; set; }   
    }
}
